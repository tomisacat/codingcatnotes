## 与 runtime 如何交互

Obj-C 通过三种方式与 runtime 交互：

* Obj-C 源码
* 定义在 Foundation NSObject 类中的方法
* 直接调用 runtime 函数

#### 源码

编译成 -> `objc_msgSend`

#### NSObject

* description
* isKindOfClass
* isMemberOfClass
* respondsToSelector
* conformToProtocol
* methodForSelector

#### runtime

`/usr/include/objc`

## Messaging

* [objc_msgSend's New Prototype](https://www.mikeash.com/pyblog/objc_msgsends-new-prototype.html)
* [Objective-C 1.0 中类与对象的定义](https://kangzubin.com/objc1.0-class-object/)
* [Creating Objective-C Classes At Runtime](https://medium.com/@husain.amri/creating-objective-c-classes-at-runtime-1f02b3a3a1db)
* [MAObjCRuntime](https://github.com/mikeash/MAObjCRuntime)
* [objc.io/The Compiler](https://www.objc.io/issues/6-build-tools/compiler/)
* [Objective-C类成员变量深度剖析](http://quotation.github.io/objc/2015/05/21/objc-runtime-ivar-access.html)
* [Objective-C Runtime](http://yulingtianxia.com/blog/2014/11/05/objective-c-runtime/)
* [Objective-C 中的类和对象](https://blog.ibireme.com/2013/11/25/objc-object/)
* [神经病院Objective-C Runtime入院第一天——isa和Class](https://www.jianshu.com/p/9d649ce6d0b8)
* [神经病院Objective-C Runtime住院第二天——消息发送与转发](https://www.jianshu.com/p/4d619b097e20)
* [神经病院Objective-C Runtime出院第三天——如何正确使用Runtime](https://www.jianshu.com/p/db6dc23834e3)
* [Objective-C 消息发送与转发机制原理](http://yulingtianxia.com/blog/2016/06/15/Objective-C-Message-Sending-and-Forwarding/)
* [objc_msgSend's New Prototype](https://www.mikeash.com/pyblog/objc_msgsends-new-prototype.html)
* [Objective-C 引用计数原理](http://yulingtianxia.com/blog/2015/12/06/The-Principle-of-Refenrence-Counting)
* [mikeash/MAObjCRuntime](https://github.com/mikeash/MAObjCRuntime)
* [深入了解Objective-C消息发送与转发过程](https://chipengliu.github.io/2019/06/02/objc-msgSend-forward/)
* [对象方法消息传递流程](https://www.desgard.com/iOS-Source-Probe/Objective-C/Runtime/objc_msgSend%E6%B6%88%E6%81%AF%E4%BC%A0%E9%80%92%E5%AD%A6%E4%B9%A0%E7%AC%94%E8%AE%B0%20-%20%E5%AF%B9%E8%B1%A1%E6%96%B9%E6%B3%95%E6%B6%88%E6%81%AF%E4%BC%A0%E9%80%92%E6%B5%81%E7%A8%8B.html)

1. `objc_msgSend`
2. `methodForSelector`

## Dynamic method resolution

@dynamic

1. `resolveInstanceMethod`
2. `resolveClassMethod`
3. `class_addMethod`

## Message forwarding

cache -> dispatch list -> resolveXX(class_addMethod) -> forwardingTargetForSelector -> methodSignatureForSelector -> forwardInvocation